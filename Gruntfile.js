module.exports = function (grunt) {
    grunt.initConfig({
        sass: {                              // Task
            dist: {
                options: {
                    style: 'compressed'
                },// Target
                files: {
                    'style/css/layout.css': 'style/layout.scss',
                    'style/css/components.css': 'style/components.scss'
                }
            },
        },
        watch: {
            css: {
                files: ['style/*.scss'],
                tasks: ['sass']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['sass']);
}