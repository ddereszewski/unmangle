'use strict';
app.directive('answer', function(){
    return {
        restrict: 'A',
        controller: ['$scope',function($scope){
            var $ctrl = this;
            var elements = [];

            this.addInput = function(inputElement){
                elements[inputElement.number] = inputElement;
            }

            this.removeInput = function(inputElement){
                var index = elements.indexOf(inputElement);
                elements.splice(index, 1);
            }

            this.inputChanged = function(){
                $scope.$ctrl.checkAnswer(getAnswer());
            }

            // we need to clean status of inputs that angular didnt move
            $scope.$watch('$ctrl.question', function(){
                elements.forEach(function(e){
                    e.letter = '';
                });
                if(elements[0])
                    elements[0].focus();
            });


            this.focusNext = function(number){
                if(elements[number+1] != undefined){
                    elements[number+1].focus();
                }
            }

            this.focusPrevious = function(number){
                if(number > 0 && elements[number-1] != undefined){
                    elements[number-1].focus();
                }
            }


            function getAnswer(){
                var answer = '';
                elements.forEach(function(e){
                    answer += (e.letter === '')? ' ' : e.letter;
                });
                return answer;
            }
        }]
    }

});