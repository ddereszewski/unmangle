'use strict';
app.component('home', {
    templateUrl: './view/home.html',
    controller: ['DataScore','$scope',function(DataScore,$scope){
        var $ctrl = this;

        DataScore.get().then(function(scores){
            $ctrl.scores = scores;
        });


    }]
});