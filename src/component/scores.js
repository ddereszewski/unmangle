'use strict';
app.component('scores', {
    templateUrl: './view/scores.html',
    controller: ['DataScore',function(DataScore){
        var $ctrl = this;

        DataScore.get().then(function(scores){
            $ctrl.scores = scores;
        });
    }]
});