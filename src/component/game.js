'use strict';
app.component('game', {
    templateUrl: './view/game.html',
    controller: ['Game','$interval','Auth','$rootRouter','DataScore','$element',function(game,$interval,auth,$rootRouter,DataScore,$element){
        var $ctrl = this;
        var timer;

        if(!auth.isAuthenticated()){
            $rootRouter.navigate(['Login']);
        }

        this.init = function(){
            $ctrl.finished = false;
            $ctrl.inProgress = false;
            $ctrl.loaded = false;
            game.init().then(function(){
                $ctrl.loaded = true;
                $ctrl.timeLeft = 40;
                $ctrl.points = 0;
            },function(){
                //:TODO implement
            });
        }

        this.start = function(){
            game.start();
            $ctrl.question = game.getQuestion();
            $ctrl.inProgress = true;

            //count down
            timer = $interval(function() {
                $ctrl.timeLeft--;

                if($ctrl.timeLeft <= 0){
                    stopTimer();
                    finish();
                }
            }, 1000);
        };

        this.checkAnswer = function(answer){
            if(game.checkAnswer(answer)){
                $ctrl.points =   game.getScore();
                $ctrl.question = game.getQuestion();
            }
        }

        this.$onDestroy = function(){
            stopTimer();
        }

        function stopTimer(){
            if (angular.isDefined(timer)) {
                $interval.cancel(timer);
                timer = undefined;
            }
        }

        function finish(){
            DataScore.add(auth.getUser(), $ctrl.points);
            $ctrl.finished = true;
            $ctrl.inProgress = false;

        }


        //init
        this.init();
        $ctrl.user = auth.getUser();
    }]
});