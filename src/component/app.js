
app.component('app', {
    templateUrl: './view/app.html',
    $routeConfig: [
        {path: '/', name: 'Home', component: 'home', useAsDefault: true},
        {path: '/login', name: 'Login', component: 'login'},
        {path: '/game', name: 'Game', component: 'game' },
        {path: '/scores', name: 'Scores', component: 'scores' }
    ]
});
