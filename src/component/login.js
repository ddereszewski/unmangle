'use strict';

app.component('login', {
    templateUrl: './view/login.html',
    controller: ['Auth','$rootRouter', '$element',function(Auth, $rootRouter, $element){
        var $ctrl = this;

        $($element).find('input').focus();

        this.start = function(){
            Auth.login($ctrl.name);
            $rootRouter.navigate(['Game']);
        }
    }]
});
