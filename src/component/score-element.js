'use strict';
app.component('scoreElement', {
    templateUrl: './view/component/score-element.html',
    bindings:{
        score: '<',
    }
});