'use strict';
app.component('answerInput', {
    templateUrl: './view/component/answer-input.html',
    bindings:{
        number: '<'
    },
    require: {
        answer: '^^answer'
    },
    controller: ['$element','$timeout',function($element,$timeout){
        this.letter = '';
        var $ctrl = this;
        var oldValue = '';

        this.$onInit = function(){
            this.answer.addInput(this);
            if(this.number == 0){
                this.focus();
            }
        };

        this.$onDestroy = function(){
            this.answer.removeInput(this)
        }

        this.keyDown = function(e){
            if(e.keyCode == 8){  // backspace
                if($ctrl.letter != ' ' ){
                    $timeout(function(){
                        $ctrl.answer.focusPrevious($ctrl.number);
                    });
                }
            }
            else if(e.keyCode == 37){ // left arrow
                $timeout(function(){
                    $ctrl.answer.focusPrevious($ctrl.number);
                });
            }
            else if(e.keyCode == 39){ // right arrow
                $timeout(function(){
                    $ctrl.answer.focusNext($ctrl.number);
                });
            }
            //ng-change doesn't detect the same value so need to hack for input change purpose
            else if(e.keyCode >= 65 && e.keyCode <= 90){
                $timeout(function() {
                    if(oldValue != $ctrl.letter){
                        $ctrl.answer.inputChanged();
                    }
                    $ctrl.answer.focusNext($ctrl.number);
                });
            }
            oldValue = this.letter;
        }

        this.focus = function(){
            $timeout(function() { // do at the end
                $($element).find('input').focus();
                $($element).find('input').select();
            });
        }

    }],

});