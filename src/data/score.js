'use strict';
/**
 * limit is removed as sorting in Firebase is ... unusual
 */
app.factory('DataScore',['$q','$http','api_base',function($q,$http,api_base) {

    return {
        /**
         * get list of  words from the database
         * @param limit
         */
        get: function () {
            var deferred = $q.defer();

            $http.get(api_base + 'scores.json').then(function(d){
                //due to text unique id returned by Firebase it has to be transformed to an array
                var results = [];
                for (var property in d.data) {
                    if (d.data.hasOwnProperty(property)) {
                        results.push(d.data[property])
                    }
                }
                deferred.resolve(results);
            },function(e){
                deferred.reject(e);
            });
            return deferred.promise;
        },

        add: function(user, score){
            var deferred = $q.defer();
            //:TODO wrong
            $http.post(api_base + 'scores.json',{
                "score": score,
                "datetime": new Date().getTime(),
                "user": user
            }).then(function(d){
                deferred.resolve();
            },function(e){
                deferred.reject(e);
            });
            return deferred.promise;
        }
    }
}]);