
app.factory('DataWord', ['$q','$http', 'api_base',function($q, $http, api_base) {

    return {
        /**
         * get list of all words from the database
         */
        get: function () {

            var deferred = $q.defer();
           // return
            $http.get(api_base + 'words.json').then(function(d){
                deferred.resolve(d.data);
            },function(e){
                deferred.reject(e);
            });

            return deferred.promise;
        }
    }
}]);