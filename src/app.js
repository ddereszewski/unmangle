/**
 * Created by dawid on 18/05/2016.
 */

var app = angular.module('app',['ngComponentRouter']);

app.config(['$locationProvider',function($locationProvider) {
    $locationProvider.html5Mode(false);
}]);
app.value('$routerRootComponent', 'app');
app.value('api_base', 'https://unmangle.firebaseio.com/');
