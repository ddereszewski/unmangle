'use strict';
app.factory('Auth', [function () {
    var authenticated = false;
    var user;

    return {
        isAuthenticated: function () {
            return authenticated;
        },
        login: function ( _name ) {
            user = _name;
            authenticated = true;
        },
        logout: function(){
            authenticated = false;
            user = null;
        },
        getUser: function(){
            if(!authenticated){
                return false;
            }
            return user;
        }
    }

}]);