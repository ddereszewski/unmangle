'use strict';

app.factory('Game', ['$q','DataWord', 'Scoring', function ($q, dataWord, scoring) {

    var words = [];
    var score = 0;
    var current = {
        question: null,
        word: null
    }

    /**
     * Gets new words and adds into existing
     * @param number - how many should be taken
     */
    function fetchWords(number){
        number = number || 50;
        var deferred = $q.defer();;
        dataWord.get(number).then(function(_words){
            _words.forEach(function(w){
               words.push(w);
            });
            shuffleWords();
            deferred.resolve();
        },function(r){
            deferred.reject(r);
        });
        return deferred.promise;
    }

    function shuffleWords(){
        var j, x, i;
        for (i = words.length; i; i -= 1) {
            j = Math.floor(Math.random() * i);
            x = words[i - 1];
            words[i - 1] = words[j];
            words[j] = x;
        }
    }

    function mangleWord(_word) {
        var a = _word.split('');
        var j, x, i;
        for (i = a.length; i; i -= 1) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
        return a.join('');
    }

    function loadNextQuestion(){
        if(words.length === 0){
            current.word = null;
            current.question = null;
            return;
        }
        var w = words.pop();
        current.word = w;
        current.question = mangleWord(current.word);

        //retry if the same
        if(current.word == current.question){
            current.question = mangleWord(current.word);
        }
    }

    return {
        // prepare a new game
        init: function(){
            score = 0;

            var deferred = $q.defer();
            fetchWords(50).then(function(){
                deferred.resolve();
            },function(r){
                deferred.reject();
                //:TODO implement
            })
            return deferred.promise;
        },

        start: function(){
            loadNextQuestion();
            scoring.init(this.getWord());
        },

        getWord: function(){
            if(current.word == null){
                throw new Error('Game not started');
            }
            return current.word;
        },

        getQuestion: function(){
            return current.question;;
        },

        getScore: function(){
            return score;
        },

        /**
         * if correct we load next and add points
         * @param answer
         * @returns {boolean}
         */
        checkAnswer: function(answer){
            if(scoring.checkAnswer(answer)){
                score += scoring.getScore();
                this.start();
                return true;
            }
            return false;
        }


    }
}]);