/**
 * Initalize object with the word user is guessing. After each change in the naswer use Set Answer to let the object calculate the score
 * It assumes answer and word string are the same length
 *
 * The	scoring	of	the	game	is	as	follows:
 * • A	full	score	is	given	only	if	the	word	was	entered	correctly	on	first	guess	without	ever
 * deleting	any	character
 * • For	each	character that	was	deleted	whilst	entering	the	word	-1	is	subtracted	from	the	score
 * • The	maximally	obtainable	score	for	a	word	is	max_score	=	floor(1.95^(n/3))where	n
 * is	the	number	of	characters	in	the	word,	^ denotes	the	exponential	function	and	floor	is	the
 * round-down	function	which	truncates	a	float	to	an	integer
 * • The	score	for	a	word	can	never	be	negative,	as	such	max_score	>= score	>= 0	always	holds	for
 *  score	being	the	actual	score	the	user	receives	for	solving	this	word
 *
 */
app.factory('Scoring', function(){

    /**
     * previous answer used to understand the change and remove points from deleting a character
     */
    var word, score, max_score, previous_answer,correct;

    function setMaxScore(word){
        max_score = Math.floor(Math.pow(1.95, (word.length / 3  )));
    }

    function isCorrect(answer){
        if(word === answer){
            correct = true;
            return true;
        }
        correct = false;
        return false;
    }

    function calculateScore(answer){
        if(typeof previous_answer != 'string'){
            previous_answer = answer;
            return;
        }
        //comapre answer to detect removed text
        answer.split('').forEach(function(el, i){
            if(el === ' ' && previous_answer[i] != ' ' ){
                removePoint();
                //if text has been changed
            }else if(previous_answer[i] != ' ' && el != ' ' && el != previous_answer[i]){
                removePoint();
            }
        });

        previous_answer = answer;
    }

    function removePoint(){
        score = Math.max(0 , score - 1);
    }

    return{
        init: function(_word){
            word = _word.toLowerCase();
            previous_answer = null;
            setMaxScore(_word);
            score = max_score;
        },
        /**
         * returns true if correct, if not recalculate the score
         */
        checkAnswer: function(_answer){

            _answer = _answer.toLowerCase();
            if(typeof word != 'string'){
                throw new Error('Init the object before checking the answer');
            }

            if(_answer.length != word.length){
                throw new Error('The answer should be the same length as the original word');
            }
            if(isCorrect(_answer)){
                return true;
            }

            calculateScore(_answer);
            return false;
        },
        getScore: function(){

            if(correct){
                return score;
            }
            return 0;
        },

        getMaxScore: function(){
            return max_score;
        }
    }

});