'use strict';

describe('Auth', function(){

    var game,deferred,$scope,dataWord;

    beforeEach(module('app'));

    beforeEach(function(){
        inject(function($injector, _$q_,_$rootScope_){;
            game = $injector.get('Game');
            dataWord = $injector.get('DataWord');
            deferred = _$q_.defer();
            $scope = _$rootScope_;
        });
    });

    it('Should load words after initialization', function(){

        var words = ["pizza","impossible","job", "work"];

        spyOn(dataWord, "get").and.callFake(function() {
            deferred.resolve(words);
            return deferred.promise;
        });

        game.init();
        $scope.$apply();
        expect(function(){game.getWord() }).toThrow(Error('Game not started'));
    });

    it('Should return question and word after start', function(){

        var words = ["pizza"];

        spyOn(dataWord, "get").and.callFake(function() {
            deferred.resolve(words);
            return deferred.promise;
        });

        game.init();
        $scope.$apply();
        game.start();
        expect(game.getWord()).toBe('pizza');

    });

    it('Should score correct answer ', function(){

        var words = ["pizza","pizza","pizza"];

        spyOn(dataWord, "get").and.callFake(function() {
            deferred.resolve(words);
            return deferred.promise;
        });

        game.init();
        $scope.$apply();
        game.start();
        expect(game.getScore()).toBe(0);
        expect(game.checkAnswer('pizza')).toBe(true);
        expect(game.getScore()).toBe(3);

        //next question
        expect(game.checkAnswer('pia  ')).toBe(false);
        expect(game.checkAnswer('piz  ')).toBe(false);
        expect(game.checkAnswer('pizza')).toBe(true);
        expect(game.getScore()).toBe(5);
    });

});
