'use strict';

describe('Auth', function(){

    var auth, dataUser, deferred,$scope;

    beforeEach(module('app'));

    beforeEach(function(){
        inject(function($injector, _$q_,_$rootScope_){
            auth = $injector.get('Auth');
            deferred = _$q_.defer();
            $scope = _$rootScope_;
        });
    });

    it('Should log in the user if the data is correct', function(){
        var user = "Bob";
        expect(auth.isAuthenticated()).toBe(false);

        auth.login(user);
        expect(auth.isAuthenticated()).toBe(true);
        expect(auth.getUser()).toBe(user);
    });

    it('Should NOT be login after logout', function(){
        expect(auth.isAuthenticated()).toBe(false);
        auth.login('wrong');
        auth.logout();
        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getUser()).toBe(false);
    });

});
