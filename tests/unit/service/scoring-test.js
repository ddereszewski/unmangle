describe('Scoring', function(){

    var scoring;

    beforeEach(module('app'));

    beforeEach(function(){
        inject(function($injector){
            scoring = $injector.get('Scoring');
        });
    });

    it('it should return 0 score without the answer', function(){
        var word = "PIZZA";
        scoring.init('PIZZA');
        expect(scoring.getScore()).toBe(0);
    });

    it('it should set max score for correct answer', function(){
        var word = "PIZZA";
        scoring.init('PIZZA');
        scoring.checkAnswer('P    ');
        scoring.checkAnswer('PI   ');
        scoring.checkAnswer('PIZ  ');
        scoring.checkAnswer('PIZZ ');
        scoring.checkAnswer('PIZZA');
        expect(scoring.getScore()).toBe(3);
    });

    it('it should remove 1 point if I made one mistake', function(){
        var word = "PIZZA";
        scoring.init('PIZZA');
        scoring.checkAnswer('P    ');
        scoring.checkAnswer('PI   ');
        scoring.checkAnswer('PID  ');
        scoring.checkAnswer('PI   ');
        scoring.checkAnswer('PIZ  ');
        scoring.checkAnswer('PIZZ ');
        scoring.checkAnswer('PIZZA');
        expect(scoring.getScore()).toBe(2);
    });

    it('it should remove 1 point if the letter was changed ', function(){
        var word = "PIZZA";
        scoring.init('PIZZA');
        scoring.checkAnswer('P    ');
        scoring.checkAnswer('PI   ');
        scoring.checkAnswer('PID  ');
        scoring.checkAnswer('PIZ  ');
        scoring.checkAnswer('PIZZ ');
        scoring.checkAnswer('PIZZA');
        expect(scoring.getScore()).toBe(2);
    });


    it('it should give max if you magically enter hole word correctly at once', function(){
        var word = "PIZZA";
        scoring.init('PIZZA');
        scoring.checkAnswer('PIZZA');
        expect(scoring.getScore()).toBe(3);
    });

    it('it should never go below 0', function(){
        var word = "PIZZA";
        scoring.init('TESTA');
        scoring.checkAnswer('TEST1');
        scoring.checkAnswer('TEST1');
        scoring.checkAnswer('TEST3');
        scoring.checkAnswer('TEST1');
        scoring.checkAnswer('TEST5');
        scoring.checkAnswer('TEST6');
        expect(scoring.getScore()).toBe(0);
    });

    it('it should throw an exception if the answer has different length', function(){
        var word = "PIZZA";
        scoring.init('TESTA');
        expect(function(){ scoring.checkAnswer('dw') }).toThrow(new Error('The answer should be the same length as the original word'));
    });

    it('it should return correct Max score value', function(){
        scoring.init('PIZZA');
        expect(scoring.getMaxScore()).toBe(3);
        scoring.init('D');
        expect(scoring.getMaxScore()).toBe(1);
        scoring.init('IMPOSSIBLE');
        expect(scoring.getMaxScore()).toBe(9);
    });

    it('it should throw an exception if not initialized', function(){

        expect(function(){ scoring.checkAnswer('test') }).toThrow( Error('Init the object before checking the answer') );

    });

    it('it should not be case sensitive', function(){
        var word = "PIZZA";
        scoring.init('PIZZA');
        scoring.checkAnswer('pizza');
        expect(scoring.getScore()).toBe(3);

    });

    // bug fix. Wrong scoring from the second answer onward
    it('it should result in one point less then max', function(){
        var word = "weekend";
        scoring.init(word);
        expect(scoring.getMaxScore()).toBe(4);
        scoring.checkAnswer('w      ');
        scoring.checkAnswer('we     ');
        scoring.checkAnswer('wek    ');
        scoring.checkAnswer('wee    ');
        scoring.checkAnswer('week   ');
        scoring.checkAnswer('weeke  ');
        scoring.checkAnswer('weeken ');
        scoring.checkAnswer('weekend');
        expect(scoring.getScore()).toBe(3);

        scoring.init(word);
        expect(scoring.getMaxScore()).toBe(4);
        scoring.checkAnswer('w      ');
        scoring.checkAnswer('we     ');
        scoring.checkAnswer('wek    ');
        scoring.checkAnswer('wee    ');
        scoring.checkAnswer('week   ');
        scoring.checkAnswer('weeke  ');
        scoring.checkAnswer('weeken ');
        scoring.checkAnswer('weekend');
        expect(scoring.getScore()).toBe(3);

    });
});
